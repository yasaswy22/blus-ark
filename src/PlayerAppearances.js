Participants.gs;

function myFunction() {
  const blusSheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  let blusSheetNames = new Array();
  let blusParticipantSheets = []; //selecting stat sheets

  let participants = [];

  const NAME_COLUMN = 2;
  const START_COUNT = 1;

  // Get participants sheet names
  for (let i = 0; i < blusSheets.length; i++) {
    if (blusSheets[i].getName().includes("Stats")) {
      blusSheetNames.push(blusSheets[i].getName());
      blusParticipantSheets.push(blusSheets[i]);
    }
  }
  console.info("blusSheetNames: " + blusSheetNames);
  console.info("blusSheetNames.length:" + blusSheetNames.length);
  console.info("blusParticipantSheets.length: " + blusParticipantSheets.length);

  let tempMaxRows;
  let tempCurrentName;
  let recordPushed=0;
  let tempCount;

  for (let i = 0; i < 5; i++) {
  // for (let i = 0; i < blusParticipantSheets.length; i++) {
    tempMaxRows = blusParticipantSheets[i].getLastRow();
    console.info("Total rows in " + blusSheetNames[i] + " is: " + tempMaxRows);
    for (let j = 1; j < tempMaxRows; j++) {
      tempCurrentName = blusParticipantSheets[i].getRange(j, NAME_COLUMN).getValue();

      if (tempCurrentName != "Name" && tempCurrentName!=""){
        if(participants.length>0) {
          for (let k=0; k<participants.length; k++){
            let tempParticipantName = participants[k]["name"];
            if(tempParticipantName==tempCurrentName){
            tempCount = participants[k]["count"]+1;
            participants[k]["count"] = tempCount;
            break;
            } else {
              recordPushed++;
              participants.push({id: recordPushed, name: tempCurrentName, count: 1});
              break;
            }
          }
        }
        else {
        recordPushed++;
        participants.push({id: recordPushed, name: tempCurrentName, count: 1});
        break;
        }
      }

    }
    console.info("processed " + i + " sheet");
  }   

  console.info("blusParticipants map array has been updated: \n");
  for (let k=0; k<participants.length; k++){
    console.info("{ id: "+participants[k]["id"] + 
                  ", name: "+participants[k]["name"] +
                  ", count: "+participants[k]["count"] +"}" );

  }
}