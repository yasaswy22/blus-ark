/** @OnlyCurrentDoc */

function changeRankColor() {
  var sheet = SpreadsheetApp.getActive().getSheetByName("Overview");
  var columnRank = 3
  var columnResult = 4
  var startRow = 32
  var endRow = 100+startRow


  for (var i=startRow; i< endRow; i++) {
    if (sheet.getRange(i, columnResult).getValue() == "Loss"){
      sheet.getRange(i, columnRank).setFontColor('red');
    }
    else if (sheet.getRange(i, columnResult).getValue() == "Win")
      sheet.getRange(i, columnRank).setFontColor('green');
    else 
      sheet.getRange(i, columnRank).setFontColor('grey');
  }
}